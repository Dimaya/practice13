"use strict";

const personalMovieDB = {
    actors: {},
    genres: [],
    privat: true, //*оставил "true" для теста //
    numberOfFilms: function () {
        let askForCount = prompt('Сколько фильмов вы уже посмотрели?', '12');
        while (askForCount == null) {
            askForCount = prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        while (askForCount == 0) {
            askForCount = prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        const count = askForCount
        personalMovieDB.count = count
    },
    movie: function () {
        let askForMovies = prompt('Один из последних просмотренных фильмов?', 'logan');
        while (askForMovies == null) {
            askForMovies = prompt('Один из последних просмотренных фильмов?', '');
        }
        while (askForMovies.length >= 50) {
            askForMovies = prompt('Один из последних просмотренных фильмов?', '');
        };
        const rating = prompt('На сколько оцените его?', '8.1')
        const movies = {
            [askForMovies]: (`${rating}`)
        };
        personalMovieDB.movies = movies;
    },
    counter: function (callback) {
        if (personalMovieDB.count <= 10) {
            alert('Просмотрено довольно мало фильмов');
        } else if (personalMovieDB.count <= 30) {
            alert('Вы классический зритель');
        } else if (personalMovieDB.count > 30) {
            alert('Вы киноман');
        } else {
            alert('Произошла ошибка');
        }
    },
    writeYourGenres: function () {
        personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        while (personalMovieDB.genres[0] == null) {
            personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        };
        while (personalMovieDB.genres[0] == 0) {
            personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        }
        personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        while (personalMovieDB.genres[1] == null) {
            personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        };
        while (personalMovieDB.genres[1] == 0) {
            personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        }
        personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        while (personalMovieDB.genres[2] == null) {
            personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        };
        while (personalMovieDB.genres[2] == 0) {
            personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        }
    },
    likeGenres: function (callback) {
        personalMovieDB.genres.forEach(function (item, i, [genres]) {
            console.log(`Любимый жанр ${i} это ${item}`)
        })
    },
    showMyDB: function () {
        // надо ли тут создавать 'callback' функцию от 'toggeVisibleMyDB'? в данном контексте я решил не задавать, 
        //но думаю если бы переменную 'privat' мы бы меняли в задании то тогда я бы делал уже через callback. //
        if (personalMovieDB.privat == false) {
            console.log(personalMovieDB);
        };
    },
    toggleVisibleMyDB: function () {
        if (personalMovieDB.privat == false) {
            personalMovieDB.privat = true;
        } else if (personalMovieDB.privat == true) {
            personalMovieDB.privat = false
        };
    },

};
personalMovieDB.numberOfFilms();
personalMovieDB.movie();
personalMovieDB.counter(personalMovieDB.numberOfFilms);
personalMovieDB.writeYourGenres();
personalMovieDB.likeGenres(personalMovieDB.writeYourGenres);
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();


// const soldier = {
//     health: 400,
//     armor: 100
// }

// const john = Object.create(soldier);
// // const john = {
// //     health:100
// // }
// // john.__proto__ = soldier;

// console.log(john.armor)

// Object.setPrototypeOf(john, soldier)








// let a = 5,
//     b = a;

// b = b+5;
// console.log(b);
// console.log(a);

// function copy(mainObj){
//     let objCopy = {};
//     let key;

//     for(key in mainObj){
//         objCopy[key] = mainObj[key];
//     }
//     return objCopy;
// }

// const number = {
//     a: 2,
//     b: 5,
//     c:{
//         x: 7,
//         y:4,
//     }
// }

// const newNumbers = copy(number);

// newNumbers.a = 10;
// console.log(newNumbers.a)
// console.log(number)

// const add = {
//     d:17,
//     e:20
// }

// console.log(Object.assign(number,add))

// const clone =Object.assign({}, add)
// clone.d = 10;

// console.log(add)
// console.log(clone)

// const oldArray = ['a', 'b','c'];
// const newArray = oldArray.slice();

// newArray[1] = 'dfghjkmnhbgvfcd'
// console.log(newArray)
// console.log(oldArray)

// const video = ['youtube', 'vimeo', 'rutube'],
//         blogs =['wordpress', 'livejournal', 'bloggs'],
//         internet = [...video, ...blogs, 'vk', 'facebook'];

// console.log(internet)

// function log(a, b, c){
//     console.log(a)
//     console.log(b)
//     console.log(c)
// }

// const numb = [2, 5, 7];

// log(...numb)

// const arr = [1, 22, 3, 67, 8];
// arr.sort(compareNum)
// console.log(arr)

// function compareNum(a, b){
//     return a-b;
// }

// arr.forEach(function (item, i, arr){
//     console.log(`${i}: ${item} внутри массива ${arr}`);
// });

// const str = prompt('','');
// const product = str.split(', ');
// product.sort()
// console.log(product.join('; '));

// arr.pop();
// arr.push(10);

// for(let i = 0; i <arr.length;i++){
//     console.log(arr[i]);
// };

// for(let value of arr){
//     console.log(value);
// }

// console.log(arr);
// const options = {
//     name: 'test',
//     width: 1024,
//     height: 1024,
//     colors: {
//         border: 'black',
//         bg: 'red'
//     },
//     makeTest: function(){
//         console.log('test');
//     }
// };

// options.makeTest();

// const {border, bg} = options.colors;

// console.log(border);

// console.log(Object.keys(options));
// console.log(options.name);

// delete options.name;

// console.log(options.name);

// let counter = 0;
// for( let key in options){
//     if (typeof (options[key]) === 'object'){
//         for( let i in options[key]){
//             console.log(`свойство ${i} имеет значение  ${options[key][i]}`);
//             counter++;
//         };
//     } else {
//     console.log(`свойство ${key} имеет значение  ${options[key]}`);
//     counter++;
//     }
// }
// console.log(counter);


// function first(){
//     setTimeout (function(){
//         console.log(1);
//     }, 500);
// }
// function second(){
//     console.log(2);
// }
// first();
// second();

// function learnJS(lang, callback){
//     console.log(`я учу: ${lang}`);
//     callback();
// }

// function done(){
//       console.log('я прошёл этот урок!')
// }

// learnJS('javaScript', done);
